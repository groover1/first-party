import random
from pprint import pprint
from typing import Dict


# Randomize data and store into Dict (typed)
concert_premiere_length = random.randint(1,15)
songs: Dict[str, int] = {}

for cnt in range(0,30):
    songs[f'music_{cnt}'] = random.randint(1,10)

# Make a single pass of the tracks, organize into a dict with key=song length
# O(n)
songs_by_len = {v: k for k, v in songs.items()}

print('songs_by_len :')
pprint(songs_by_len)

print('songs :')
pprint(songs)

print(f'concert_premiere_length : {concert_premiere_length}')

# double loop to get the first two songs, calculate the remainder and fetch from dict we made
# O(n^2)
def check():
    for first, first_len in songs.items():
        for second, second_len in songs.items():
            remaining_len = concert_premiere_length - first_len - second_len
            if remaining_len in songs_by_len and (first != second != songs_by_len[remaining_len]):
                print(first, second, songs_by_len[remaining_len])
                return True
    return False


# o(n) + O(n^2) = O(n^2)
print(check())
